
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	//Setting Speed of how fast ships can move
	SetSpeed(100);
	//How much health the ship has
	SetMaxHitPoints(1);
	//Hitbox around the enemy Ship
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	// Is Enemy Ship Active?
	if (IsActive())
	{
		// Settings x as a float and making it get total time
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		// Settings x to get speed 
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		// Is position and get speed
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
