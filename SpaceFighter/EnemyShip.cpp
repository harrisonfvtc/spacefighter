
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	// Setting Enemy Ship to 1 HP
	SetMaxHitPoints(1);
	// Setting Collision Radius or "HitBox" to 20 px
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) // Is delay greater than 0?
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); // delay seconds = delay seconds - Elapsed Time

		if (m_delaySeconds <= 0) // Is delaySeconds less than or equal to 0?
		{
			GameObject::Activate(); // Activate GameObject
		}
	}

	if (IsActive()) // Is Active?
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); // Activation Seconds = Activation Seconds + Get Time Elapsed
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); // if Activation Seconds > 2 and Is on screen is false Deactivate
	}

	Ship::Update(pGameTime); // Update Ship
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}