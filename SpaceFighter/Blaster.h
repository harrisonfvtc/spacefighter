
#pragma once

#include "Weapon.h"
#include "Projectile.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.3; // Speed of weapon
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const
	{
		return m_cooldown <= 0;
	}

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	int userAmmo = 0;
	float ShortCooldown = 0.2;
	float LongCooldown = 2;

	virtual void Fire(TriggerType triggerType)
	{
			if (IsActive() && CanFire())
			{
				if (triggerType.Contains(GetTriggerType()))
				{
					if (userAmmo <= 10)
					{
						userAmmo++;
						Projectile* pProjectile = GetProjectile();
						if (pProjectile)
						{
							pProjectile->Activate(GetPosition(), true);
							m_cooldown = m_cooldownSeconds;
						}
						if (userAmmo == 10)
						{
							m_cooldown = LongCooldown;
							userAmmo = 0;
						}
						else
						{
							m_cooldown = ShortCooldown;
						}
					}
			}
		}
	}


public:

	float m_cooldown;
	float m_cooldownSeconds;

};